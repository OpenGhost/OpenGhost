// Fill out your copyright notice in the Description page of Project Settings.

#include "BaseCharacterMovementComponent.h"
#include "BaseCharacter.h"

float UBaseCharacterMovementComponent::GetMaxSpeed() const
{
	float MaxSpeed = Super::GetMaxSpeed();

	const ABaseCharacter* character = Cast<ABaseCharacter>(PawnOwner);
	if (character)
	{
		if (character->IsAiming() || character->IsCrouching() || character->IsWalking())
			MaxSpeed = character->myWalkSpeed;
		else if (character->IsSprinting())
			MaxSpeed = FMath::Lerp(character->myJogSpeed, character->mySprintSpeed, FVector::DotProduct(character->GetVelocity().GetSafeNormal(), character->GetActorForwardVector()));
		else
			MaxSpeed = character->myJogSpeed;
	}

	return MaxSpeed;
}


