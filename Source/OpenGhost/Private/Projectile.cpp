#include "Projectile.h"

#include "Components/CapsuleComponent.h"
#include "Components/StaticMeshComponent.h"
#include "GameFramework/ProjectileMovementComponent.h"
#include "Net/UnrealNetwork.h"
#include "BaseCharacter.h"

// Sets default values
AProjectile::AProjectile()
	: myKnockbackScale(1000.0f)
{
	PrimaryActorTick.bCanEverTick = true;
	bReplicates = true;

	myCollisionComponent = CreateDefaultSubobject<UCapsuleComponent>(TEXT("CapsuleComponent"));
	myCollisionComponent->BodyInstance.SetCollisionProfileName("Projectile");
	//TODO Test both OnHit and Overlap. Depends on how accurate hit detection stays with high velocities.

	myCollisionComponent->OnComponentBeginOverlap.AddDynamic(this, &AProjectile::OnBeginOverlap);
	myCollisionComponent->OnComponentHit.AddDynamic(this, &AProjectile::OnHit);
	myCollisionComponent->SetWalkableSlopeOverride(FWalkableSlopeOverride(WalkableSlope_Unwalkable, 0.f));
	myCollisionComponent->CanCharacterStepUpOn = ECB_No;
	RootComponent = myCollisionComponent;

	myProjectileMovementComponent = CreateDefaultSubobject<UProjectileMovementComponent>(TEXT("ProjectileComponent"));
	myProjectileMovementComponent->UpdatedComponent = myCollisionComponent;
	myProjectileMovementComponent->bRotationFollowsVelocity = true;
	myProjectileMovementComponent->bShouldBounce = true;

	myMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MeshComponent"));
	myMesh->SetupAttachment(myCollisionComponent);
	myMesh->SetIsReplicated(true);
}

void AProjectile::Launch(FVector Direction, float LaunchSpeedModifier /*= 1.0f*/)
{
	float totalSpeed = myProjectileMovementComponent->InitialSpeed * LaunchSpeedModifier;
	float initialSpeed = FMath::Min(totalSpeed, myProjectileMovementComponent->MaxSpeed);
	myProjectileMovementComponent->InitialSpeed = initialSpeed;
}

void AProjectile::BeginPlay()
{
	Super::BeginPlay();
	
}

float AProjectile::CalculateDamage() const
{
	//TODO introduce more nuances like damage fallof
	return myDamage;
}

void AProjectile::DealDamage(ABaseCharacter* TargetCharacter, UPrimitiveComponent* TargetComponent, const FHitResult& HitResult)
{
	FPointDamageEvent damageEvent;
	damageEvent.Damage = CalculateDamage();
	damageEvent.HitInfo = HitResult;
	damageEvent.ShotDirection = myProjectileMovementComponent->Velocity.GetSafeNormal() * myKnockbackScale;
	damageEvent.DamageTypeClass = myDamageType;

	TargetCharacter->TakeDamage(CalculateDamage(), damageEvent, Instigator->GetController(), this);
}

void AProjectile::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AProjectile::Reset()
{
	Super::Reset();

	Destroy();
}

void AProjectile::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(AProjectile, myMesh);
}

void AProjectile::OnBeginOverlap(
	UPrimitiveComponent* OverlappedComponent, 
	AActor* OtherActor, 
	UPrimitiveComponent* OtherComponent,
	int32 OtherBodyIndex, 
	bool bFromSweep, 
	const FHitResult & SweepResult)
{
	if (!HasAuthority())
		return;

	if (OtherActor && OtherActor != this && OtherComponent && OtherActor != GetOwner() && OtherActor != Instigator)
	{
		if (auto character = static_cast<ABaseCharacter*>(OtherActor))
		{
			FHitResult Hit;

			if (bFromSweep)
				Hit = SweepResult;
			else
				OtherComponent->LineTraceComponent(Hit, GetActorLocation() - GetVelocity() * 10.0, GetActorLocation() + GetVelocity(), 
					FCollisionQueryParams(GetClass()->GetFName(), OtherComponent->bTraceComplexOnMove, this));

			DealDamage(character, OtherComponent, Hit);
		}
		//TODO test if slow enough for destruction
		Destroy();
	}
}


void AProjectile::OnHit(UPrimitiveComponent* OurComponent, AActor* OtherActor, UPrimitiveComponent* OtherComponent, FVector NormalImpulse, const FHitResult& Hit)
{
	if (!HasAuthority())
		return;

	if (OtherActor && OtherActor != this && OtherComponent && OtherActor != GetOwner() && OtherActor != Instigator)
	{
		if (auto character = static_cast<ABaseCharacter*>(OtherActor))
		{
			DealDamage(character, OtherComponent, Hit);
		}
		//TODO test if slow enough for destruction
		Destroy();
	}
}